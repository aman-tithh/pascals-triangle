// javascript
var x = PascalsTriangle(10);
document.write(x);

function PascalsTriangle(n){ //function calculates Pascals Triangle for the given nth row

///////////// 
        if (n < 0){ //if n is less than zero return an empty array
        return PascalArray = Array(1)
    }
/////////////
    else if (n == 0){ //if n is equal to 0 return a single index array that is equal to 1
        return PascalArray = Array(n+1).fill(1)
    }
////////////
    var PascalArray = Array(n+1).fill(1) //initialize an array with n+1 indices and set them equal to 1
    PascalArray[1] = n; //set the 1st index equal to n
    PascalArray[n-1] = n; //set the 2nd last index equal to n
    
    for(var i = 2; i<Math.ceil((n+1)/2); i++){
        var j = i -1; // set j = i -1
        PascalArray[i] = PascalArray[i] = PascalArray[j]*(n-j)/i // formula is derived from pascal trianlges facotorial formula
        PascalArray[n-i] = PascalArray[j]*(n-j)/i // as the pascal traingle is symmetrical this sets the other half of the triangle to the correct values
    }
    return PascalArray  
}

